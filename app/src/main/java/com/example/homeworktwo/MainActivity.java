package com.example.homeworktwo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    String []jobs = {"Actor", "Teacher", "Developer", "Footballer", "Song Chanmoni"};
    Button buttonSave,buttonUpload;
    RadioButton rbButtonChoose;
    RadioGroup radioGroup;
    private Spinner spinner;
    ImageView imageView;
    int SELECT_PICTURE = 200;

    EditText etHeader, etEmail, etWebsite, etSpinner;
    boolean isAllFieldsChecked = false;
    User user=new User();
    private final static int REQUEST_CODE_1 = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        Spinner spinner = (Spinner) findViewById(R.id.mySpinner);
        // Create an ArrayAdapter
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
        // Specify the layout
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                Log.d("TAG", "onItemClick: " + jobs[position]);
                Toast.makeText(getApplicationContext(), jobs[position], Toast.LENGTH_SHORT).show();
                user.setJob(jobs[position]);
                Log.d("Profile", "onItemSelected: " + user.job);
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d("TAG", "onItemClick: Nothing to do");
            }
        });

        buttonSave = findViewById(R.id.btnSave);
        etHeader = findViewById(R.id.txtHeader);
        etEmail = findViewById(R.id.txtEmail);
        etWebsite = findViewById(R.id.txtWebsite);
        buttonUpload = findViewById(R.id.btnUpload);
        imageView = findViewById(R.id.imageView);
//        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);

        buttonUpload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageChooser();
            }
        });

        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                int selectedId = radioGroup.getCheckedRadioButtonId();
//
//                radioGroup = (RadioGroup) findViewById(selectedId);
//                Log.d("TAG", "onClickListenerSaved: " + rbButtonChoose.getText());

                isAllFieldsChecked = CheckAllFields();

                if (isAllFieldsChecked) {
                    user.setHeader(etHeader.getText().toString());
                    user.setEmail(etEmail.getText().toString());
                    user.setWebsite(etWebsite.getText().toString());
//                    user.setRdButton(rbButtonChoose.getText().toString());

                    Intent intent = new Intent(MainActivity.this, SaveActivity.class);
                    intent.putExtra("Header", user.header);
                    intent.putExtra("Email", user.email);
                    intent.putExtra("Website", user.website);
                    intent.putExtra("Job", user.job);
//                    intent.putExtra("Option", user.rdButton);
                    startActivityForResult(intent,REQUEST_CODE_1 );
                }
            }
        });
        }
        //radiobutton
    public void onclickbuttonMethod(View v){
        int selectedId = radioGroup.getCheckedRadioButtonId();
        rbButtonChoose = (RadioButton) findViewById(selectedId);
        if (selectedId == -1) {
            Toast.makeText(MainActivity.this, "សូមរើសប្រភព", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, rbButtonChoose.getText(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean CheckAllFields() {
        if (etHeader.length() == 0) {
            etHeader.setError("ត្រូវដាក់ប្រធានបទ");
            return false;
        }

        if (etWebsite.length() == 0) {
            etWebsite.setError("ត្រូវដាក់ Link វេបសាយ");
            return false;
        }

        if (etEmail.length() == 0) {
            etEmail.setError("ត្រូវដាក់អុីម៉ែលរបស់អ្នក");
            return false;
        }

        //radioButton
        if(radioGroup.getCheckedRadioButtonId()==-1)
        {
            Toast.makeText(getApplicationContext(), "សូមជ្រើសរើសប្រភព", Toast.LENGTH_SHORT).show();
        }
        else
        {

            int selectedId = radioGroup.getCheckedRadioButtonId();

            rbButtonChoose = (RadioButton)findViewById(selectedId);
            Toast.makeText(getApplicationContext(), rbButtonChoose.getText().toString()+" is selected", Toast.LENGTH_SHORT).show();
        }

        return true;
    }

    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_1:
                if (resultCode == RESULT_OK) {
                    String messageReturn = data.getStringExtra("message_return");
                    Context context = getApplicationContext();
                    CharSequence text = messageReturn;
                    int duration = Toast.LENGTH_SHORT;

                    Toast toast = Toast.makeText(context, text, duration);
                    toast.show();

                    if (etHeader != null && etEmail != null && etWebsite != null && etSpinner != null) {
                        etHeader.setText("");
                        etEmail.setText("");
                        etWebsite.setText("");
                        etSpinner.setText("");
                        
                    }

                }
        }

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                // Get the url of the image from data
                Uri selectedImageUri = data.getData();
                if (null != selectedImageUri) {
                    // update the preview image in the layout
                    imageView.setImageURI(selectedImageUri);
                }
            }

        }
    }

    void imageChooser() {

        Intent i = new Intent();
        i.setType("image/*");
        i.setAction(Intent.ACTION_GET_CONTENT);

        startActivityForResult(Intent.createChooser(i, "Select Picture"), SELECT_PICTURE);
    }

//    public void SendImage(View view) {
//        Intent intent = new Intent(MainActivity.this, SaveActivity.class);
//        intent.putExtra("resId", R.id.imageView);
//        startActivity(intent);
//    }

}