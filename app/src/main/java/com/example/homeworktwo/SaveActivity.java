package com.example.homeworktwo;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class SaveActivity extends AppCompatActivity {

    TextView dpHeader, dpEmail, dpWebsite,dpSpinner,dpRadio;
    ImageView dpView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save);

        dpHeader = findViewById(R.id.dpHeader);
        dpEmail = findViewById(R.id.dpEmail);
        dpWebsite = findViewById(R.id.dpLink);
        dpSpinner = findViewById(R.id.dpJob);
//        dpRadio = findViewById(R.id.dpType);

        Intent intent = getIntent();
        String header = intent.getStringExtra("Header");
        String email = intent.getStringExtra("Email");
        String website = intent.getStringExtra("Website");
        String job = intent.getStringExtra("Job");
//        String rdButton = intent.getStringExtra("Type");

        dpHeader.setText(header);
        dpEmail.setText(email);
        dpWebsite.setText(website);
        dpSpinner.setText(job);
//        dpRadio.setText(rdButton);

        Button passDataTargetReturnDataButton = findViewById(R.id.btnBack);
        passDataTargetReturnDataButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.putExtra("message_return", "Please create a new Information");
                setResult(RESULT_OK, intent);
                finish();
            }
        });

//        dpView = findViewById(R.id.image);
//        Bundle bundle = getIntent().getExtras();
//        if (bundle != null) {
//            int resId = bundle.getInt("resId");
//            dpView.setImageResource(resId);
//        }

    }
}