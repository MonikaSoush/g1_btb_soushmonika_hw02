package com.example.homeworktwo;

public class User {
    String header;
    String email;
    String website;
    String job;
    String rdButton;
    String imgView;

    public String getImgView() {
        return imgView;
    }

    public void setImgView(String imgView) {
        this.imgView = imgView;
    }

    public String getRdButton() {
        return rdButton;
    }

    public void setRdButton(String rdButton) {
        this.rdButton = rdButton;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }
}
